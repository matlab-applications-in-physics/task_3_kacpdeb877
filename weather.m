%MATLAB R2020b
%name:weather
%author:kacpdeb877
%date: 23.11.2020
%version: v1.0

clc; clear;

%wzór na temperature odczuwalną (wikipedia)
%t_odczuwalna = 13.12 + (0.6215*T) - (11.37 * (V^0.16)) + (0.3965 * T * (V^0.16))


station1_imgw = 'https://danepubliczne.imgw.pl/api/data/synop/station/katowice/format/json';
station2_imgw = 'https://danepubliczne.imgw.pl/api/data/synop/station/bielskobiala/format/json';
station3_imgw = 'https://danepubliczne.imgw.pl/api/data/synop/station/raciborz/format/json';
station1 = 'http://wttr.in/katowice?format=j1';
station3 = 'http://wttr.in/raciborz?format=j1';
station2 = 'http://wttr.in/bielsko-biala?format=j1';

station1_imgw = webread(station1_imgw);
station2_imgw = webread(station2_imgw);
station3_imgw = webread(station3_imgw);
station1 = webread(station1);
station2 = webread(station2);
station3 = webread(station3);



temp1 = str2double(station1_imgw.temperatura);
wind1 = str2double(station1_imgw.predkosc_wiatru); %m/s
wind1 = wind1 * 3.6; %km/h

temp_katowice1 = 13.12 + (0.6215*temp1) - (11.37 * (wind1^0.16)) + (0.3965 * temp1 * (wind1^0.16));

temp2 = str2double(station2_imgw.temperatura);
wind2 = str2double(station2_imgw.predkosc_wiatru); %m/s
wind2 = wind2 * 3.6; %km/h

temp_bielsko1 = 13.12 + (0.6215*temp2) - (11.37 * (wind2^0.16)) + (0.3965 * temp2 * (wind2^0.16));

temp3 = str2double(station3_imgw.temperatura);
wind3 = str2double(station3_imgw.predkosc_wiatru); %m/s
wind3 = wind3 * 3.6; %km/h

temp_raciborz1 = 13.12 + (0.6215*temp3) - (11.37 * (wind3^0.16)) + (0.3965 * temp3 * (wind3^0.16));


stacja = {station1_imgw.stacja;
    station2_imgw.stacja;
    station3_imgw.stacja
    station1_imgw.stacja;
    station2_imgw.stacja;
    station3_imgw.stacja};

data = {station1_imgw.data_pomiaru;
    station2_imgw.data_pomiaru;
    station3_imgw.data_pomiaru;
    station1.weather(1).date;
    station2.weather(1).date;
    station3.weather(1).date};

godzina = {station1_imgw.godzina_pomiaru;
    station2_imgw.godzina_pomiaru;
    station3_imgw.godzina_pomiaru;
    station1.current_condition.observation_time;
    station2.current_condition.observation_time;
    station3.current_condition.observation_time};

temperatura = {station1_imgw.temperatura;
    station2_imgw.temperatura;
    station3_imgw.temperatura
    station1.current_condition.temp_C;
    station2.current_condition.temp_C;
    station3.current_condition.temp_C};

temperatura_odczuwalna = {temp_katowice1;
    temp_bielsko1;
    temp_raciborz1;
    station1.current_condition.FeelsLikeC;
    station2.current_condition.FeelsLikeC;
    station3.current_condition.FeelsLikeC};

predkosc_wiatru = {wind1;
    wind2;
    wind3;
    station1.current_condition.windspeedKmph;
    station2.current_condition.windspeedKmph;
    station3.current_condition.windspeedKmph};

kierunek_wiatru = {station1_imgw.kierunek_wiatru;
    station2_imgw.kierunek_wiatru;
    station3_imgw.kierunek_wiatru;
    station1.current_condition.winddirDegree;
    station2.current_condition.winddirDegree;
    station3.current_condition.winddirDegree};

wilgotnosc = {station1_imgw.wilgotnosc_wzgledna;
    station2_imgw.wilgotnosc_wzgledna;
    station3_imgw.wilgotnosc_wzgledna;
    station1.current_condition.humidity;
    station2.current_condition.humidity;
    station3.current_condition.humidity;};

cisnienie = {station1_imgw.cisnienie;
    station2_imgw.cisnienie;
    station3_imgw.cisnienie;
    station1.current_condition.pressure;
    station2.current_condition.pressure;
    station3.current_condition.pressure};




tablica = table(stacja, data, godzina, temperatura, temperatura_odczuwalna, predkosc_wiatru, kierunek_wiatru, wilgotnosc, cisnienie);

writetable(tablica,'weather_table.csv')
